package main

import (
	"fmt"

	"gitlab.com/dpixel/dp-cirno/models/cirno"
	"gitlab.com/dpixel/dp-cirno/models/configuration"
	"gitlab.com/dpixel/dp-cirno/models/dataset"
)

func main() {
	configuration, err := configuration.New()
	if err != nil {
		fmt.Println("Failed to get the configuration.")
		return
	}

	dataset, err := dataset.New(configuration)
	if err != nil {
		fmt.Println("Failed to generate the dataset.")
		return
	}

	cirno, err := cirno.New(configuration)
	if err != nil {
		fmt.Println("Failed to create Cirno.")
		return
	}

	accuracy, err := cirno.Train(dataset)
	if err != nil {
		fmt.Println("Failed to train Cirno.")
		return
	}
	fmt.Println("Accuracy:", accuracy)

	accuracyOnNewDataset, err := cirno.Verify()
	if err != nil {
		fmt.Println("Failed to verify the dataset by Cirno.")
		return
	}

	fmt.Println("Accuracy on the new dataset:", accuracyOnNewDataset)

	cirno.Save("./cirno.v.0.0.2.nya")
}
