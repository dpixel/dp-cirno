module gitlab.com/dpixel/dp-cirno

go 1.20

require (
	github.com/goml/gobrain v0.0.0-20201212123421-2e2d98ca8249
	github.com/montanaflynn/stats v0.7.0
	gonum.org/v1/gonum v0.12.0
)
