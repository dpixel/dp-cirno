package main

import (
	"math"
	"math/rand"

	"github.com/montanaflynn/stats"
)

type Curve struct {
	NumberOfAnomalies               int
	Points                          int
	IndexesOfCoordinatesWithAnomaly []int
	Values                          []float64
	NoisedValues                    []float64
	AnomaliedValues                 []float64
}

func New(numberOfAnomalies, points int) Curve {
	return Curve{
		NumberOfAnomalies: numberOfAnomalies,
		Points:            points,
	}
}

func generateIntInRange(min, max int) int {
	return rand.Intn(max-min) + min
}

func generateFloat64InRange(min, max float64) float64 {
	return min + rand.Float64()*(max-min)
}

func addNoiseToValue(value, standardDeviation float64) float64 {
	min := value - standardDeviation
	max := value + standardDeviation

	return generateFloat64InRange(min, max)
}

func addAnomalyToValue(value, standardDeviation float64) float64 {
	var anomaliedValue float64
	if generateIntInRange(0, 2) == 0 {
		anomaliedValue = generateFloat64InRange(value-3.0*standardDeviation, value-5.0*standardDeviation)
	} else {
		anomaliedValue = generateFloat64InRange(value+3.0*standardDeviation, value+5.0*standardDeviation)
	}

	return anomaliedValue
}

func main() {
	curve := New(10, 300)
	step := 0.1
	coordinate := 0.0

	for i := 0; i < curve.Points; i++ {
		value := math.Sin(coordinate)
		curve.Values = append(curve.Values, value)
		coordinate += step
	}

	standardDeviation, _ := stats.StandardDeviation(curve.Values)

	for i := 0; i < curve.Points; i++ {
		curve.NoisedValues = append(curve.NoisedValues, addNoiseToValue(curve.Values[i], standardDeviation))
		curve.AnomaliedValues = append(curve.AnomaliedValues, curve.NoisedValues[i])
	}

	for i := 0; i < curve.NumberOfAnomalies; i++ {
		index := generateIntInRange(0, curve.Points)
		curve.AnomaliedValues[index] = addAnomalyToValue(curve.NoisedValues[index], standardDeviation)
	}
	/*
		f, _ := os.Create("./data.txt")
		for i := 0; i < curve.Points; i++ {
			f.WriteString(fmt.Sprintf("%d\t%f\t%f\t%f\n", i, curve.Values[i], curve.NoisedValues[i], curve.AnomaliedValues[i]))
			coordinate += step
		}
	*/
}
