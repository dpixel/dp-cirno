SHELL:=/bin/bash

ROOT=${shell pwd}
GO=${shell which go}

APP_FILE=${ROOT}/cmd/app/main.go

.PHONY:
run:
	@${GO} run ${APP_FILE}