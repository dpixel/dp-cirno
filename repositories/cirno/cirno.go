package cirno

import "gitlab.com/dpixel/dp-cirno/repositories/dataset"

type Cirno interface {
	Train(dataset dataset.Dataset) (float64, error)
	Verify() (float64, error)
	Save(path string) error
}
