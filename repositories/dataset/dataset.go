package dataset

type Dataset interface {
	GetSets() [][][]float64
	GetValues(index int) []float64
	GetType(index int) float64
}
