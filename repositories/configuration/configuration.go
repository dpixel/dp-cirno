package configuration

type Configuration interface {
	GetNumberOfDatasets() int
	GetNumberOfPointsInDataset() int
	GetNumberOfHiddenLayers() int
	GetNumberOfLearningEpochs() int
	GetMaximumNumberOfAnomalies() int
	GetLearningRate() float64
	GetScaleFactor() float64
	GetAccuracy() float64
}
