package cirno

import (
	"errors"
	"math"

	"github.com/goml/gobrain"
	"github.com/goml/gobrain/persist"
	"gitlab.com/dpixel/dp-cirno/models/dataset"
	repocirno "gitlab.com/dpixel/dp-cirno/repositories/cirno"
	"gitlab.com/dpixel/dp-cirno/repositories/configuration"
	repodataset "gitlab.com/dpixel/dp-cirno/repositories/dataset"
)

type cirno struct {
	NeuralNetwork gobrain.FeedForward
	Configuration configuration.Configuration
}

func New(configuration configuration.Configuration) (repocirno.Cirno, error) {
	if configuration == nil {
		return nil, errors.New("invalid configuration")
	}

	neuralNetwork := gobrain.FeedForward{}
	neuralNetwork.Init(configuration.GetNumberOfPointsInDataset(), configuration.GetNumberOfHiddenLayers(), 1)

	return &cirno{
		NeuralNetwork: neuralNetwork,
		Configuration: configuration,
	}, nil
}

func (cirno *cirno) Train(dataset repodataset.Dataset) (float64, error) {
	if dataset == nil {
		return 0.0, errors.New("invalid dataset")
	}

	cirno.NeuralNetwork.Train(dataset.GetSets(), cirno.Configuration.GetNumberOfLearningEpochs(), cirno.Configuration.GetLearningRate(), cirno.Configuration.GetScaleFactor(), false)

	numberOfErrors := 0
	for i := 0; i < cirno.Configuration.GetNumberOfDatasets(); i++ {
		out := cirno.NeuralNetwork.Update(dataset.GetValues(i))[0]
		if math.Abs(out-dataset.GetType(i)) > cirno.Configuration.GetAccuracy() {
			numberOfErrors++
		}
	}

	return float64(numberOfErrors) * 100.0 / float64(cirno.Configuration.GetNumberOfDatasets()), nil
}

func (cirno cirno) Verify() (float64, error) {
	dataset, err := dataset.New(cirno.Configuration)
	if err != nil {
		return 0.0, err
	}

	numberOfErrors := 0
	for i := 0; i < cirno.Configuration.GetNumberOfDatasets(); i++ {
		out := cirno.NeuralNetwork.Update(dataset.GetValues(i))[0]
		if math.Abs(out-dataset.GetType(i)) > 0.05 {
			numberOfErrors++
		}
	}

	return float64(numberOfErrors) * 100.0 / float64(cirno.Configuration.GetNumberOfDatasets()), nil
}

func (cirno cirno) Save(path string) error {
	if err := persist.Save(path, cirno.NeuralNetwork); err != nil {
		return err
	}

	return nil
}
