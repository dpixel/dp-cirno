package configuration

import (
	"encoding/json"
	"io"
	"os"

	repoconfiguration "gitlab.com/dpixel/dp-cirno/repositories/configuration"
)

type configuration struct {
	NumberOfDatasets         int     `json:"number of datasets"`
	NumberOfPointsInDataset  int     `json:"number of points in dataset"`
	NumberOfHiddenLayers     int     `json:"number of hidden layers"`
	NumberOfLearningEpochs   int     `json:"number of learning epochs"`
	MaximumNumberOfAnomalies int     `json:"maximum number of anomalies"`
	LearningRate             float64 `json:"learning rate"`
	ScaleFactor              float64 `json:"scale factor"`
	Accuracy                 float64 `json:"accuracy"`
}

func New() (repoconfiguration.Configuration, error) {
	configurationFile, err := os.Open("settings/configuration.json")
	if err != nil {
		return nil, err
	}
	defer configurationFile.Close()

	bytes, err := io.ReadAll(configurationFile)
	if err != nil {
		return nil, err
	}

	var configuration configuration
	if err := json.Unmarshal(bytes, &configuration); err != nil {
		return nil, err
	}

	return &configuration, nil
}

func (configuration configuration) GetNumberOfDatasets() int {
	return configuration.NumberOfDatasets
}

func (configuration configuration) GetNumberOfPointsInDataset() int {
	return configuration.NumberOfPointsInDataset
}

func (configuration configuration) GetNumberOfHiddenLayers() int {
	return configuration.NumberOfHiddenLayers
}

func (configuration configuration) GetNumberOfLearningEpochs() int {
	return configuration.NumberOfLearningEpochs
}

func (configuration configuration) GetMaximumNumberOfAnomalies() int {
	return configuration.MaximumNumberOfAnomalies
}

func (configuration configuration) GetLearningRate() float64 {
	return configuration.LearningRate
}

func (configuration configuration) GetScaleFactor() float64 {
	return configuration.ScaleFactor
}

func (configuration configuration) GetAccuracy() float64 {
	return configuration.Accuracy
}
