package curve

import (
	"math"
	"math/rand"

	"github.com/montanaflynn/stats"
	"gonum.org/v1/gonum/floats"
)

type Properties struct {
	NumberOfAnomalies int
	Points            int
	Frequency         float64
}

type Curve struct {
	IndexesOfCoordinatesWithAnomaly []int
	Values                          []float64
	Properties                      Properties
	StandardDeviation               float64
}

func generateIntInRange(min, max int) int {
	return rand.Intn(max-min) + min
}

func generateFloat64InRange(min, max float64) float64 {
	return min + rand.Float64()*(max-min)
}

func addNoiseToValue(value, standardDeviation float64) float64 {
	min := value - standardDeviation
	max := value + standardDeviation

	return generateFloat64InRange(min, max)
}

func addAnomalyToValue(value, standardDeviation float64) float64 {
	var anomaliedValue float64
	if generateIntInRange(0, 2) == 0 {
		anomaliedValue = generateFloat64InRange(value-standardDeviation, value-25.0*standardDeviation)
	} else {
		anomaliedValue = generateFloat64InRange(value+standardDeviation, value+25.0*standardDeviation)
	}

	return anomaliedValue
}

func New(properties *Properties) Curve {
	var curve Curve
	step := 0.1
	coordinate := 0.0

	for i := 0; i < properties.Points; i++ {
		value := math.Sin(properties.Frequency * coordinate)
		curve.Values = append(curve.Values, value)
		coordinate += step
	}

	curve.Properties = *properties
	curve.StandardDeviation, _ = stats.StandardDeviation(curve.Values)

	return curve
}

func (curve *Curve) NormalizeValues() {
	min := floats.Min(curve.Values)
	max := floats.Max(curve.Values)
	var normalizedValues []float64
	for _, value := range curve.Values {
		normalizedValues = append(normalizedValues, (value-min)/(max-min))
	}

	curve.Values = normalizedValues
}

func (curve *Curve) AddNoiseValues() {
	for i := 0; i < curve.Properties.Points; i++ {
		curve.Values[i] = addNoiseToValue(curve.Values[i], curve.StandardDeviation)
	}

	curve.NormalizeValues()
}

func (curve *Curve) AddAnomaliesValues() {
	for i := 0; i < curve.Properties.NumberOfAnomalies; i++ {
		index := generateIntInRange(0, curve.Properties.Points)
		curve.Values[index] = addAnomalyToValue(curve.Values[index], curve.StandardDeviation)
	}

	curve.NormalizeValues()
}

func NewNoisedValues(properties *Properties) Curve {
	curve := New(properties)

	curve.AddNoiseValues()

	return curve
}

func NewAnomaliedValues(properties *Properties) Curve {
	curve := NewNoisedValues(properties)

	curve.AddAnomaliesValues()

	return curve
}
