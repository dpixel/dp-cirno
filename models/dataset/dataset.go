package dataset

import (
	"errors"

	"gitlab.com/dpixel/dp-cirno/models/curve"
	"gitlab.com/dpixel/dp-cirno/repositories/configuration"
	repodataset "gitlab.com/dpixel/dp-cirno/repositories/dataset"
)

const (
	ANOMALY = 1
	VANILLA = 0
)

type dataset struct {
	Sets [][][]float64
}

func New(configuration configuration.Configuration) (repodataset.Dataset, error) {
	if configuration == nil {
		return nil, errors.New("invalid configuration")
	}
	var dataset dataset

	maxFrequency := float64(1)
	stepFrequency := maxFrequency * 0.01
	for i := 0; i < configuration.GetNumberOfDatasets(); {
		for numberOfAnomalies := 1; numberOfAnomalies <= configuration.GetMaximumNumberOfAnomalies(); numberOfAnomalies++ {
			for frequency := stepFrequency; frequency <= maxFrequency; frequency += stepFrequency {
				properties := curve.Properties{
					NumberOfAnomalies: numberOfAnomalies,
					Points:            configuration.GetNumberOfPointsInDataset(),
					Frequency:         frequency,
				}

				{
					curve := curve.NewNoisedValues(&properties)
					dataset.Sets = append(dataset.Sets, [][]float64{curve.Values, {float64(VANILLA)}})
					i++
				}

				{
					curve := curve.NewAnomaliedValues(&properties)
					dataset.Sets = append(dataset.Sets, [][]float64{curve.Values, {float64(ANOMALY)}})
					i++
				}
			}
		}
	}

	return &dataset, nil
}

func (dataset dataset) GetSets() [][][]float64 {
	return dataset.Sets
}

func (dataset dataset) GetValues(index int) []float64 {
	return dataset.Sets[index][0]
}

func (dataset dataset) GetType(index int) float64 {
	return dataset.Sets[index][1][0]
}
